package dev.lonami.calnex.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;

import java.util.List;

import dev.lonami.calnex.DailyTask;
import dev.lonami.calnex.R;

public class DailyTaskAdapter extends ArrayAdapter<DailyTask> {
    private List<DailyTask> mTasks;
    Context mContext;

    private static class ViewHolder {
        TextView name;
        TextView timespan;
        TextView fullname;
        TextView room;
        TextView lecturer;
    }

    public DailyTaskAdapter(List<DailyTask> tasks, Context context) {
        super(context, R.layout.daily_task_item, tasks);
        this.mTasks = tasks;
        this.mContext=context;
    }

    @SuppressLint("ViewHolder") // not unconditional, there's an early return
    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        ViewHolder holder;
        if (convertView != null) {
            holder = (ViewHolder) convertView.getTag();
        } else {
            holder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.daily_task_item, parent, false);
            holder.name = convertView.findViewById(R.id.task_name);
            holder.timespan = convertView.findViewById(R.id.time_range);
            holder.fullname = convertView.findViewById(R.id.full_name);
            holder.room = convertView.findViewById(R.id.location);
            holder.lecturer = convertView.findViewById(R.id.lecturer);
            convertView.setTag(holder);
        }

        DailyTask task = mTasks.get(position);
        holder.name.setText(task.name);
        holder.timespan.setText(task.timespan.toString());
        holder.fullname.setText(task.fullname);
        holder.room.setText(task.room);
        holder.lecturer.setText(task.lecturer);

        return convertView;
    }
}
