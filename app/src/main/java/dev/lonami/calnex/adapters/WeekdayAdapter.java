package dev.lonami.calnex.adapters;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.ListFragment;

import java.util.ArrayList;
import java.util.List;

import dev.lonami.calnex.DailyTask;
import dev.lonami.calnex.R;
import dev.lonami.calnex.model.Faculty;
import dev.lonami.calnex.model.Subject;

public class WeekdayAdapter extends FragmentPagerAdapter {
    private static final int WEEKDAY_COUNT = 7;

    private Faculty mFaculty;

    public WeekdayAdapter(FragmentManager fm, Faculty faculty) {
        super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        mFaculty = faculty;
    }

    @Override
    public int getCount() {
        return WEEKDAY_COUNT;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        ArrayList<DailyTask> tasks = new ArrayList<>();
        for (Subject subject : mFaculty.getSubjects()) {
            tasks.add(subject.asDailyTask());
        }

        return ArrayListFragment.newInstance(position, tasks);
    }

    public static class ArrayListFragment extends ListFragment {
        int mWeekday;
        List<DailyTask> mTasks;

        static ArrayListFragment newInstance(int weekday, ArrayList<DailyTask> tasks) {
            ArrayListFragment f = new ArrayListFragment();

            Bundle args = new Bundle();
            args.putInt("weekday", weekday);
            args.putParcelableArrayList("tasks", tasks);
            f.setArguments(args);

            return f;
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            if (getArguments() == null) {
                mWeekday = 0;
                mTasks = new ArrayList<>(0);
            } else {
                mWeekday = getArguments().getInt("weekday");
                mTasks = getArguments().getParcelableArrayList("tasks");
            }
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View v = inflater.inflate(R.layout.fragment_pager_list, container, false);
            View tv = v.findViewById(R.id.text);
            ((TextView) tv).setText(getResources().getStringArray(R.array.weekday_names)[mWeekday]);
            return v;
        }


        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
            setListAdapter(new DailyTaskAdapter(mTasks, getContext()));
        }

        @Override
        public void onListItemClick(ListView l, View v, int position, long id) {
            Log.i("FragmentList", "Item clicked: " + id);
        }
    }
}
