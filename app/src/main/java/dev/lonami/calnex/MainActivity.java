package dev.lonami.calnex;

import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager.widget.ViewPager;

import org.json.JSONArray;
import org.json.JSONException;

import dev.lonami.calnex.adapters.WeekdayAdapter;
import dev.lonami.calnex.model.Faculty;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    Faculty mFaculty;
    WeekdayAdapter mAdapter;
    ViewPager mPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        try {
            JSONArray faculties = new JSONArray(Utils.readString(getResources().openRawResource(R.raw.data)));
            mFaculty = new Faculty(faculties.getJSONObject(0));
            Log.i(TAG, "Loaded faculty successfully: " + mFaculty);

            mAdapter = new WeekdayAdapter(getSupportFragmentManager(), mFaculty);
            mPager = findViewById(R.id.pager);
            mPager.setAdapter(mAdapter);
        } catch (JSONException e) {
            Log.e(TAG, "Failed to load faculties", e);
        }
    }
}
