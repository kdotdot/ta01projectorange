package dev.lonami.calnex;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.nio.charset.Charset;

public class Utils {
    public static String readString(InputStream is) {
        Reader reader = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
        Writer writer = new StringWriter();

        try {
            int n;
            char[] buffer = new char[16 * 1024];
            while ((n = reader.read(buffer)) != -1) {
                writer.write(buffer, 0, n);
            }
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } finally {
            try {
                reader.close();
            } catch (IOException ignored) {
            }
        }

        return writer.toString();
    }

    /**
     * Returns the English name for a weekday.
     * <p>
     * NOTE: You should only use this for debugging purposes, it's **not**
     * meant to be used in the UI because that needs to be locale-specific.
     *
     * @param weekday the day of the week (0 for Monday, 1 for Tuesday, etc.)
     * @return the English name for the day of the week
     */
    public static String weekdayName(int weekday) {
        switch (weekday) {
            case 0:
                return "Monday";
            case 1:
                return "Tuesday";
            case 2:
                return "Wednesday";
            case 3:
                return "Thursday";
            case 4:
                return "Friday";
            case 5:
                return "Saturday";
            case 6:
                return "Sunday";
            default:
                return "?";
        }
    }
}
