package dev.lonami.calnex.model;

import androidx.annotation.NonNull;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Locale;

public class Teacher {
    private String id;
    private String name;
    private String lastname;
    private String email;
    private Room office;

    public Teacher(JSONObject json, HashMap<String, Room> rooms) throws JSONException {
        this.id = json.getString("id");
        this.name = json.getString("name");
        this.lastname = json.getString("lastname");
        this.email = json.getString("email");
        this.office = rooms.get(json.getString("office"));
    }

    public String getId() {
        return this.id;
    }

    public String getName() {
        return name;
    }

    @NonNull
    @Override
    public String toString() {
        return String.format(Locale.ENGLISH, "[%s] %s %s (%s) at %s",
                this.id, this.name, this.lastname, this.email, this.office);
    }
}
