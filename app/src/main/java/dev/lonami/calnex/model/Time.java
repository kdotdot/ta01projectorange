package dev.lonami.calnex.model;

import androidx.annotation.NonNull;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Locale;

import dev.lonami.calnex.Utils;

public class Time {
    enum Type {
        MASTER_CLASS,
        LABORATORY,
        SEMINAR
    }

    private int weekday;
    private int hour;
    private int minute;
    private int duration;
    private Room room;
    private Type type;

    public Time(JSONObject json, HashMap<String, Room> rooms) throws JSONException {
        this.weekday = json.getInt("weekday");
        this.hour = json.getInt("weekday");
        this.minute = json.getInt("weekday");
        this.duration = json.getInt("weekday");

        this.room = rooms.get(json.getString("room"));

        switch (json.getString("type")) {
            default:
            case "master":
                this.type = Type.MASTER_CLASS;
                break;
            case "lab":
                this.type = Type.LABORATORY;
                break;
            case "seminar":
                this.type = Type.SEMINAR;
                break;
        }
    }

    @NonNull
    @Override
    public String toString() {
        return String.format(Locale.ENGLISH, "Every %s at %d:%d (for %d minutes) in %s",
                Utils.weekdayName(this.weekday), this.hour, this.minute, this.duration, this.room);
    }
}
