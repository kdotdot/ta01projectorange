package dev.lonami.calnex.model;

import androidx.annotation.NonNull;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class Faculty {
    private String name;
    private String url;

    private List<Subject> subjects;
    private HashMap<String, Teacher> teachers;
    private HashMap<String, Room> rooms;

    public Faculty(JSONObject json) throws JSONException {
        this.name = json.getString("faculty");
        this.url = json.getString("url");

        this.rooms = new HashMap<>();
        JSONArray rooms = json.getJSONArray("rooms");
        for (int i = 0; i < rooms.length(); ++i) {
            Room room = new Room(rooms.getJSONObject(i));
            this.rooms.put(room.getId(), room);
        }

        this.teachers = new HashMap<>();
        JSONArray teachers = json.getJSONArray("teachers");
        for (int i = 0; i < teachers.length(); ++i) {
            Teacher teacher = new Teacher(teachers.getJSONObject(i), this.rooms);
            this.teachers.put(teacher.getId(), teacher);
        }

        this.subjects = new ArrayList<>();
        JSONArray subjects = json.getJSONArray("subjects");
        for (int i = 0; i < subjects.length(); ++i) {
            this.subjects.add(new Subject(subjects.getJSONObject(i), this.teachers, this.rooms));
        }
    }

    @NonNull
    @Override
    public String toString() {
        return String.format(Locale.ENGLISH, "%s (%s), with %d subjects, %d teachers and %d rooms",
                this.name, this.url, this.subjects.size(), this.teachers.size(), this.rooms.size());
    }

    public List<Subject> getSubjects() {
        return subjects;
    }
}
