package dev.lonami.calnex.model;

import androidx.annotation.NonNull;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;

public class Room {
    private String id;
    private String name;
    private String building;

    public Room(JSONObject json) throws JSONException {
        this.id = json.getString("id");
        this.name = json.getString("name");
        this.building = json.getString("building");
    }

    public String getId() {
        return this.id;
    }

    @NonNull
    @Override
    public String toString() {
        return String.format(Locale.ENGLISH, "[%s] %s in %s",
                this.id, this.name, this.building);
    }
}
